#!/bin/zsh

'
This snippet is for setting up a new ArchLinux instance/vm from scratch with the minimal/defaults from a magnet available at:
https://archlinux.org/download/

Complete wiki: https://wiki.archlinux.org/index.php/Installation_guide#Network_configuration

Make sure to adjust line 108 for your cpu/gpu replacing intel-ucode for amd-ucode mesa xf86-video-amdgpu.
Change nvidia-lts if you intend on using linux-lts.
Remove intel-ucode or amd-ucode if you intend on using VirtualBox and add virtualbox-guest-utils xf86-video-vmware virtualbox-guest-modules-arch mesa mesa-libgl.

Author: Michael J. Acosta
Date: January 24 2021
Release: 2021.01.01

Edited: 7/27/2021
Potential Changes: Convert into an automated script or create an installer.
'

#-----------------------------------
#         Keyboard Layout
#-----------------------------------
# Check for available keyboard layouts
ls /usr/share/kbd/keymaps/**/*.map.gz | more
# Select the preferred keyboard
loadkeys us.map

# Check network connectivity
ip addr show
ping -c 5 1.1.1.1
# For wireless configurations
wifi-menu
# if you don't have an ip address run dhcp client
dhcpcd

#-----------------------------------
#        Disk Partitioning
#-----------------------------------
# lsblk to check disk name and gdisk to create new parttions
lsblk
gdisk /dev/nvme0n1
n # For new boot EFI partition
# First Sector leave default
+200M # Last Sector 
ef00 # for EFI system partition
n # for LVM partition
# First Sector leave default
# Last Sector leave default
8e00

p # to preview changes
w # to write changes

#-----------------------------------
#           Encryption
#-----------------------------------

cryptsetup luksFormat /dev/nvme0n1p2 # Setup Encryption and create passphrase
cryptsetup open --type=luks /dev/nvme0n1p2 lvm # Open it and enter passphrase created, this will map /dev/nvme0n1p2 to /dev/mapper/lvm

#-----------------------------------
#   Physical and Volume Groups     
#-----------------------------------

pvcreate /dev/mapper/lvm # Create physical volume on the mapped drive
vgcreate vg1 /dev/mapper/lvm # Create volume group on the mapped drive

#-----------------------------------
#        Logical Volumes
#-----------------------------------

lvcreate -L 200GB vg1 -n lv_root #For root
lvcreate -l 100%FREE vg1 -n lv_home #For home

#-----------------------------------
#        Format the LV's
#-----------------------------------

# Format the logical volumes lv_root and lv_home
mkfs.fat -F32 /dev/nvme0n1p1
mkfs.btrfs /dev/vg1/lv_root
mkfs.btrfs /dev/vg1/lv_home

#-----------------------------------
#       Mount /boot and LV's
#-----------------------------------

# Mount root
mount /dev/vg1/lv_root /mnt

#Create paths for boot and home
mkdir -p /mnt/{boot,home}

# Mount /boot
mount /dev/nvme0n1p1 /mnt/boot
# Mount home
mount /dev/vg1/lv_home /mnt/home

#Confirm with mount that all 3 of them are mounted
mount | tail -n 3


#-----------------------------------
#  INSTALLING ARCH LINUX
#-----------------------------------

# Base system install
pacstrap /mnt base linux linux-firmware intel-ucode vim lvm2 #do linux-lts linux-headers-lts for stable kernels

# Generate FSTAB
genfstab -U -p /mnt >> /mnt/etc/fstab
#Verify FSTAB
cat /mnt/etc/fstab

# Chroot (Switch from the live iso to the local install environment)
arch-chroot /mnt



#-----------------------------------
#             Packages
#-----------------------------------
#Enable multilib
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
pacman -S linux-headers grub efibootmgr dosfstools os-prober mtools dnsutils networkmanager network-manager-applet wpa_supplicant wireless_tools netctl dialog base-devel sudo xorg xorg-server logrotate reflector mlocate bluez bluez-utils pulseaudio-bluetooth pavucontrol blueman cups alacritty archlinux-wallpaper nitrogen libgl mesa nvidia nvidia-utils nvidia-libgl zsh vim openssh git xdg-utils xdg-user-dirs python python3 python-pipenv neofetch thunderbird gnome-keyring steam lightdm lightdm-gtk-greeter xfce4 xfce4-goodies thunar xarchiver gvfs-smb qtile lxappearance qt5ct ttf-roboto

#-----------------------------------
#      Date Time & Localization
#-----------------------------------

# Set localtime
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
# Set hardware clock
hwclock --systohc
# Set the local(uncomment en_US.UTF-8 UTF-8)
vim /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 >> /etc/locale.conf

#-----------------------------------
#         Hostname
#-----------------------------------
vim /etc/hostname # Set hostname
'
box-dev
'
vim /etc/hosts # Use example below
'
127.0.0.1   localhost
::1         localhost
127.0.0.1   box-dev.localdomain box-dev
'

#-----------------------------------
#          Users & Groups
#-----------------------------------

passwd # Change root password

useradd -m -g users -G wheel,audio,video,optical,storage,plugdev michael # Make a new user
passwd michael # Create pass for new usr

EDITOR=vim visudo # Configure sudoers groups and uncomment the wheel line %wheel ALL=(ALL) NOPASSWD: ALL under '## Same thing without a password'


#-----------------------------------
#         Boot Configuration
#-----------------------------------
#Generate ramdisk
vim /etc/mkinitcpio.conf
# Find the "HOOKS" line (line #52 or thereabouts), add "encrypt lvm2" in between "block" and "filesystems"
# It should look similar to the following (don't copy this line in case they change it, but just add the two new items):
'HOOKS=(base udev autodetect modconf block encrypt lvm2 filesystems keyboard keymap fsck)'
# Now go for it ;)
mkinitcpio -p linux # do linux-lts for stable
# GRUB install (UEFI)
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
# Get the UUID of the encrypted(/dev/nvme0n1p2) volume using blkid of TYPE="crypto_LUKS"
blkid
# add UUID to /etc/default/grub using vim on line 7
'
GRUB_CMDLINE_LINUX="cryptdevice=UUID=35258219-ef48-4c84-8a5a-2562c295443e:lvm root=/dev/vg1/lv_root" 
'
# make grub conf
grub-mkconfig -o /boot/grub/grub.cfg

#-----------------------------------
#         Enable Services
#-----------------------------------
#Setup ssh, networking, others to autostart
systemctl enable NetworkManager sshd lightdm cups bluetooth 


exit
umount -a
reboot

#-----------------------------------
#            .xprofile
#-----------------------------------
vim ~/.xprofile
'
setxkbmap us
nitrogen --restore
picom -f &
xrandr --output DP-0 --mode 2560x1440 --rate 143.96
'

#-----------------------------------
#              lightdm
#-----------------------------------
sudo vim /etc/lightdm/lightdm.conf #at line 114 uncomment
'
display-setup-script=xrandr --output DP-0 --mode 2560x1440 --rate 143.96
'


#-----------------------------------
#               PARU
#-----------------------------------
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

#Additional pkgbuild install
paru google-chrome
paru octopi
paru bitwig-studio
paru 1password
paru 1password-cli
#Microsoft
paru microsoft-edge-stable-bin
#Razer
paru openrazer-meta
paru polychromatic

#-----------------------------------
#               PICOM
#-----------------------------------
paru picom-ibhagwan-git
mkdir -p $HOME/.config/picom
curl -o $HOME/.config/picom/picom.conf https://github.com/ibhagwan/picom/blob/next-rebase/picom.sample.conf

#-----------------------------------
#             ALACRITTY
#-----------------------------------
mkdir -p $HOME/.config/alacritty
curl -o $HOME/.config/alacritty/alacritty.yml https://gitlab.com/dwt1/dotfiles/-/raw/master/.config/alacritty/alacritty.yml

#-----------------------------------
#               EXA
#-----------------------------------
paru exa-git
echo "" >> $HOME/.zshrc
echo "#EXA: Replacement for ls written in Rust" >> $HOME/.zshrc
echo "alias l='exa'" >> $HOME/.zshrc
echo "alias la='exa -a'" >> $HOME/.zshrc
echo "alias ll='exa -abghHliS'" >> $HOME/.zshrc
echo "alias ls='exa --color=auto'" >> $HOME/.zshrc
echo "alias lg='exa --long --grid'" >> $HOME/.zshrc
echo "alias lt='exa --long --tree'" >> $HOME/.zshrc

source $HOME/.zshrc

#-----------------------------------
#         VMWARE WORKSTATION
#-----------------------------------
paru vmware-workstation
sudo rmmod vmw_vmci vmmon; 
sudo modprobe -a vmw_vmci vmmon; 
sudo systemctl restart vmware-networks.service; 
sudo systemctl restart vmware-usbarbitrator.service;
sudo systemctl enable vmware-networks.service; 
sudo systemctl enable vmware-usbarbitrator.service;

#-----------------------------------
#             PROTONUP
#-----------------------------------
pip3 install protonup
protonup -d "~/.steam/root/compatibilitytools.d/"
echo " " >> $HOME/.zshrc
echo "#Export PATH" >> $HOME/.zshrc
echo "export PATH=\$HOME/.local/bin:\$PATH" >> $HOME/.zshrc
source $HOME/.zshrc

#-----------------------------------
#             SNAPD
#-----------------------------------
paru -S snapd --noconfirm
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install snap-store
sudo snap install powershell --classic
sudo snap install code --classic
