- Author: Michael J. Acosta
- Date: January 24 2021
- Tested: ArchLinux Release: 2021.11.01

Guided snippet for setting up a new 
ArchLinux instance/vm from scratch with the 
essentials for a production workstation.

ISO available from https://archlinux.org/download/

Complete wiki: https://wiki.archlinux.org/index.php/Installation_guide#Network_configuration

Make sure to adjust according for your cpu/gpu replacing intel-ucode and nvidia for amd-ucode mesa xf86-video-amdgpu.
Change nvidia-lts if you intend on using linux-lts.
Remove intel-ucode or amd-ucode if you intend on using VirtualBox and add virtualbox-guest-utils xf86-video-vmware virtualbox-guest-modules-arch mesa mesa-libgl.

