"""
                      ,----,           ,--,              
                    ,/   .`|        ,---.'|              
    ,----..       ,`   .'  :   ,---,|   | :       ,---,. 
   /   /   \    ;    ;     /,`--.' |:   : |     ,'  .' | 
  /   .     : .'___,/    ,' |   :  :|   ' :   ,---.'   | 
 .   /   ;.  \|    :     |  :   |  ';   ; '   |   |   .' 
.   ;   /  ` ;;    |.';  ;  |   :  |'   | |__ :   :  |-, 
;   |  ; \ ; |`----'  |  |  '   '  ;|   | :.'|:   |  ;/| 
|   :  | ; | '    '   :  ;  |   |  |'   :    ;|   :   .' 
.   |  ' ' ' :    |   |  '  '   :  ;|   |  ./ |   |  |-, 
'   ;  \; /  |    '   :  |  |   |  ';   : ;   '   :  ;/| 
 \   \  ',  . \   ;   |.'   '   :  ||   ,/    |   |    \ 
  ;   :      ; |  '---'     ;   |.' '---'     |   :   .' 
   \   \ .'`--"             '---'             |   | ,'   
    `---`                                     `----'     
                                                                  
Author: Michael J. Acosta
Date: 11/30/2021
Qtile Version: 0.18.1-3     
     
"""

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.layout import columns
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

super = "mod4"
alt = "mod1"
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([super], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([super], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([super], "j", lazy.layout.down(), desc="Move focus down"),
    Key([super], "k", lazy.layout.up(), desc="Move focus up"),
    Key([super], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([super, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([super, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([super, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([super, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([super, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([super, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([super, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([super, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([super], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([super, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([super], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    #Kill -9
    Key([alt, "control"], "Escape", lazy.spawn("xkill"), desc="Kill X11 Window"),

    #Screenshots
    Key([alt, "shift"], "3", lazy.spawn("xfce4-screenshooter -f -c"), desc="Screenshot: Fullscreen"),
    Key([alt, "shift"], "4", lazy.spawn("xfce4-screenshooter -r -c"), desc="Screenshot: Region"),
    Key([alt, "shift"], "5", lazy.spawn("xfce4-screenshooter -w -c"), desc="Screenshot: Window"),
   

    # Toggle between different layouts as defined below
    Key([super], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([super], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([super, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([super, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([super], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([alt], "r", lazy.spawn("launcher_slate"), desc="rofi launcher"),
    Key([super], "Escape", lazy.spawn("powermenu"), desc="rofi powermenu"),
]

#groups = [Group(i) for i in "123456789"]
groups =[]

group_names = ["1","2","3","4","5","6","7","8","9"]

#group_labels = ["1","2","3","4","5","6","7","8","9"]
group_labels = ["📏","📝","📌","🔍","📗","📎","🔑","🧬","🔓"]
#group_layouts = ["Web","Dev","Edit","Mail","Chat","Video","Game"]

group_layouts = ["columns","max","monadtall","monadtall","monadtall","monadtall","monadtall","monadtall","monadtall"]

for i in range(len(group_names)):
        groups.append(
            Group(
                name=group_names[i],
                layout=group_layouts[i].lower(),
                label=group_labels[i],

            )
        )

for i in groups:
    keys.extend([
        # alt + letter of group = switch to group
        Key([super], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # alt + shift + letter of group = switch to & move focused window to group
        Key([super, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # alt + shift + letter of group = move focused window to group
        # Key([super, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Columns(
        border_focus=['#03d1ff'],
        border_normal=['#8103ff'],
        border_width=4,
        margin=20
        ),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    layout.Stack(num_stacks=2),
    layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    layout.Tile(),
    layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]


widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=10,
)
extension_defaults = widget_defaults.copy()

barcolor='#167fb8'
barbordercolor="#7300c4"
widgetbackground="#621aab"
workspaceicon=16
iconsize=18

screens = [
    Screen(
        top=bar.Bar(
            [   
                
                widget.Sep(linewidth=40,background=barcolor,foreground=barcolor),
                widget.CurrentLayoutIcon(scale=0.5),
                widget.TextBox(),
                widget.GroupBox(active="#fcdb03",disable_drag=True,fontsize=workspaceicon),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(),

                #widget.TextBox(text='✉️',fontsize=15,padding=1),
                #widget.GmailChecker(username='mikey.j.acosta@gmail.com',password='qclscuxtevpzurtb',padding=1),
                
                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='💎',fontsize=iconsize,padding=1),
                widget.CryptoTicker(crypto='ETH',format='{amount:.2f}',padding=1),
                
                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='🔲',fontsize=iconsize,padding=1),
                widget.CPU(format='{freq_current}GHz {load_percent}%',padding=1),

                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='🧽',fontsize=iconsize,padding=1),
                widget.Memory(measure_mem='G',padding=1),
                #widget.HDDGraph(),
                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.DF(visible_on_warn=True,partition='/home'),
                widget.DF(visible_on_warn=True,partition='/'),

                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='⚗️',fontsize=iconsize,padding=1),
                widget.NvidiaSensors(padding=4),

                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='📈',fontsize=iconsize,padding=1),
                widget.Net(interface='eno2',format='{down}↓↑{up}',use_bits=True,padding=1),

                widget.Sep(linewidth=5,background=barcolor,foreground=barcolor),
                widget.TextBox(text='⌛️',fontsize=iconsize,padding=1),
                widget.Clock(format='%I:%M %p',padding=1),
                widget.Sep(linewidth=40,background=barcolor,foreground=barcolor),
            ],
            42,
            background=barcolor,
            border_color=[barbordercolor,barbordercolor,barbordercolor,barbordercolor],
            border_width=[80, 80, 80, 80],
            margin=[15, 20, 0, 20],
            opacity=1
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([super], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([super], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([super], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
